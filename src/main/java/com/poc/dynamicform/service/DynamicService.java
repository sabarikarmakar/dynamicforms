package com.poc.dynamicform.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.poc.dynamicform.dto.DynamicFormDto;
import com.poc.dynamicform.model.Response;

public interface DynamicService {

	DynamicFormDto getDynamicForm(String name);

	ResponseEntity<Response> saveDynamicForm(DynamicFormDto dynamicFormdto);

	List<DynamicFormDto> getAllDynamicForms();

	ResponseEntity<Response> deleteDynamicForm(String name);

	ResponseEntity<Response> updateDynamicForm( DynamicFormDto dynamicFormDto);

}
