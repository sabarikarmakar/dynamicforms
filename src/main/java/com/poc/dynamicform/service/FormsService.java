package com.poc.dynamicform.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.poc.dynamicform.dto.SubmitFormsDto;
import com.poc.dynamicform.model.Response;

public interface FormsService {

	ResponseEntity<Response> submitForm(SubmitFormsDto forms);

	List<SubmitFormsDto> getAllSubmittedForms();

	SubmitFormsDto getForm(String id);

	ResponseEntity<Response> deleteSubmittedForm(String id);

}
