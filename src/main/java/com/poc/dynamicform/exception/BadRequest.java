package com.poc.dynamicform.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.poc.dynamicform.model.Response;

@ControllerAdvice
public class BadRequest {
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public ResponseEntity<Response> showCustomMessage() {
		Response response = new Response(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST.name(),
				"Something went wrong",null);

		return new ResponseEntity<>(response, HttpStatus.valueOf(HttpStatus.BAD_REQUEST.value()));
	}

}
