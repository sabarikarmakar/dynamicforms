package com.poc.dynamicform.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import com.poc.dynamicform.enums.Status;
import com.poc.dynamicform.model.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.poc.dynamicform.dto.SubmitFormsDto;
import com.poc.dynamicform.entity.SubmitForms;
import com.poc.dynamicform.repo.FormsRepo;
import com.poc.dynamicform.service.FormsService;

@Service
public class FormsServiceImpl implements FormsService {

    @Autowired
    private FormsRepo formRepo;

    @Autowired
    private ConvertService converterService;

    @Override
    public ResponseEntity<Response> submitForm(SubmitFormsDto forms) {

        SubmitForms submitForms = converterService.convertToEntity(forms);
        
       

        formRepo.save(submitForms);
        Response resp = new Response(HttpStatus.OK.value(), "OK", "Saved Successfully", submitForms);
        return ResponseEntity.ok().body(resp);

    }

    @Override
    public List<SubmitFormsDto> getAllSubmittedForms() {

        List<SubmitForms> submitForms = formRepo.getAllSubmittedForms();
        return submitForms.stream().map(converterService::convertToDto).collect(Collectors.toList());
    }

    @Override
    public SubmitFormsDto getForm(String id) {

        SubmitForms submitForms = formRepo.findByFormId(id);

        return converterService.convertToDto(submitForms);
    }

    @Override
    public ResponseEntity<Response> deleteSubmittedForm(String id) {

        SubmitForms submitForms = formRepo.findByFormId(id);

        Response resp = new Response();

        if (submitForms.getStatus().equals(Status.ENABLED)) {
            submitForms.setStatus(Status.DISABLED);
            formRepo.save(submitForms);
            resp = new Response(HttpStatus.OK.value(), "OK", "Form with " + id + " Deleted Successfully"
                    , null);

        } else {
            resp = new Response(HttpStatus.BAD_REQUEST.value(), "Bad Request", "Problem Encountered While Deleting!",
                    null);
        }


        return ResponseEntity.ok().body(resp);
    }

}
