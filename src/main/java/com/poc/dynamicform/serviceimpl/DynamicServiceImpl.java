package com.poc.dynamicform.serviceimpl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.poc.dynamicform.dto.DynamicFormDto;
import com.poc.dynamicform.entity.DynamicForm;
import com.poc.dynamicform.enums.Status;
import com.poc.dynamicform.model.Response;
import com.poc.dynamicform.repo.DynamicRepo;
import com.poc.dynamicform.service.DynamicService;

@Service
public class DynamicServiceImpl implements DynamicService {

    @Autowired
    private DynamicRepo repo;

    @Autowired
    private ConvertService converterService;

    @Override
    public DynamicFormDto getDynamicForm(String name) {
        DynamicForm dynamicForm = repo.findByName(name);
        return converterService.convertToDto(dynamicForm);
    }

    @Override
    public ResponseEntity<Response> saveDynamicForm(DynamicFormDto dynamicFormDto) {

        DynamicForm dynamicForm = converterService.convertToEntity(dynamicFormDto);
        Response resp = new Response();

        if (dynamicForm.getName().isEmpty()) {

            resp = new Response(HttpStatus.BAD_REQUEST.value(), "Bad Request", "Name is Null", null);
            return ResponseEntity.ok().body(resp);

        } else {

            if (!repo.existsDynamicFormByName(dynamicForm.getName())) {
                repo.save(dynamicForm);
                resp = new Response(HttpStatus.OK.value(), "OK", "Saved Successfully", dynamicForm);

            } else {

                resp = new Response(HttpStatus.BAD_REQUEST.value(), "Bad Request", "Name Already Exits", null);

            }
        }

        return ResponseEntity.ok().body(resp);
    }

    @Override
    public List<DynamicFormDto> getAllDynamicForms() {
        List<DynamicForm> dynamicForm = repo.getAllDynamicForms();

        return dynamicForm.stream().map(converterService::convertToDto).collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<Response> deleteDynamicForm(String name) {

        DynamicForm dForm = repo.findByName(name);
        Response resp = new Response();

        if (dForm.getStatus().equals(Status.ENABLED)) {
            dForm.setStatus(Status.DISABLED);
            repo.save(dForm);
            resp = new Response(HttpStatus.OK.value(), "OK", "Dynamic Form " + name + " Deleted Successfully", null);

        } else {
            resp = new Response(HttpStatus.BAD_REQUEST.value(), "Bad Request", "Problem Encountered While Deleting!",
                    null);
        }


        return ResponseEntity.ok().body(resp);
    }

    @Override
    public ResponseEntity<Response> updateDynamicForm(DynamicFormDto dynamicFormDto) {

        DynamicForm dynamicForm = converterService.convertToEntity(dynamicFormDto);

        DynamicForm dForm = repo.findByName(dynamicForm.getName());
        Response resp = new Response();

        if (dForm.getStatus().equals(Status.ENABLED)) {
            dForm.setField(dynamicForm.getField());

            repo.save(dForm);
            resp = new Response(HttpStatus.OK.value(), "OK",
                    "Dynamic Form " + dynamicForm.getName() + " Updated Successfully", dForm);

        } else {
            resp = new Response(HttpStatus.BAD_REQUEST.value(), "Bad Request", "Problem Encountered While Updating!",
                    null);

        }

        return ResponseEntity.ok().body(resp);
    }

}
