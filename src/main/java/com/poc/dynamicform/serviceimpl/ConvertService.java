package com.poc.dynamicform.serviceimpl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.poc.dynamicform.dto.DynamicFormDto;
import com.poc.dynamicform.dto.SubmitFormsDto;
import com.poc.dynamicform.entity.DynamicForm;
import com.poc.dynamicform.entity.SubmitForms;

@Component
public class ConvertService {

	@Autowired
	private ModelMapper modelMapper;

	public SubmitFormsDto convertToDto(SubmitForms submitForms) {
		return modelMapper.map(submitForms, SubmitFormsDto.class);
	}

	public SubmitForms convertToEntity(SubmitFormsDto submitFormsDto) {
		return modelMapper.map(submitFormsDto, SubmitForms.class);
	}

	public DynamicFormDto convertToDto(DynamicForm dynamicForm) {
		return modelMapper.map(dynamicForm, DynamicFormDto.class);
	}

	public DynamicForm convertToEntity(DynamicFormDto dynamicFormDto) {
		return modelMapper.map(dynamicFormDto, DynamicForm.class);
	}

}
