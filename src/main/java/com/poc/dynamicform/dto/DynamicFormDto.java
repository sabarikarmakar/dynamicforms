package com.poc.dynamicform.dto;

import java.util.List;

import com.poc.dynamicform.enums.Status;
import com.poc.dynamicform.model.Fields;

import lombok.Data;

@Data
public class DynamicFormDto {

	private String _id;

	private String name;

	private Status status;

	private List<Fields> field;

}
