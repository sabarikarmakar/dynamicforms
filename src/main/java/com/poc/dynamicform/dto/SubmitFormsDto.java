package com.poc.dynamicform.dto;

import java.util.List;

import com.poc.dynamicform.enums.Status;
import com.poc.dynamicform.model.Fields;

import lombok.Data;

@Data
public class SubmitFormsDto {
	
	private String formId;

	private String submittedBy;

	private String formName;

	private Status status;
	
	private List<Fields> field;
	
}
