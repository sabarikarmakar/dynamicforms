package com.poc.dynamicform.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.poc.dynamicform.dto.SubmitFormsDto;
import com.poc.dynamicform.model.Response;
import com.poc.dynamicform.service.FormsService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/forms")
public class FormsController {

	@Autowired
	private FormsService service;

	@ApiOperation(value = "Submit Form Details", nickname = "SubmitForm")
	@PostMapping("/submit")
	public ResponseEntity<Response> submitForm(@RequestBody SubmitFormsDto forms) {

		return service.submitForm(forms);
	}

	@ApiOperation(value = "Get All Submitted Forms", nickname = "GetAllSubmittedForms")
	@GetMapping("/getAllSubmittedForms")
	public List<SubmitFormsDto> getAllSubmittedForms() {
		return service.getAllSubmittedForms();
	}

	@ApiOperation(value = "Delete Form", nickname = "DeleteFormByName")
	@DeleteMapping("/delete")
	public ResponseEntity<Response> deleteData(@RequestParam String id) {

		return service.deleteSubmittedForm(id);

	}

	@ApiOperation(value = "Get Form By ID", nickname = "GetFormByID")
	@GetMapping("/getForm")
	public SubmitFormsDto getForm(@RequestParam String id) {
		return service.getForm(id);
	}

}
