package com.poc.dynamicform.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.poc.dynamicform.dto.DynamicFormDto;
import com.poc.dynamicform.model.Response;
import com.poc.dynamicform.service.DynamicService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/dynamicform")
public class DynamicController {

	@Autowired
	private DynamicService service;

	@ApiOperation(value = "Create Form", nickname = "CreateForm")
	@PostMapping("/createForm")
	public ResponseEntity<Response> createForm(@RequestBody DynamicFormDto dynamicFormDto) {
		return service.saveDynamicForm(dynamicFormDto);
	}

	@ApiOperation(value = "Get Form By Name", nickname = "GetFormByName")
	@GetMapping("/get")
	public DynamicFormDto getData(@RequestParam String name) {

		return service.getDynamicForm(name);

	}

	@ApiOperation(value = "Delete Form", nickname = "DeleteFormByName")
	@DeleteMapping("/delete")
	public ResponseEntity<Response> deleteData(@RequestParam String name) {

		return service.deleteDynamicForm(name);

	}

	@ApiOperation(value = "Get All Forms", nickname = "GetAllForms")
	@GetMapping("/getAll")
	public List<DynamicFormDto> getAllDynamicForms() {
		return service.getAllDynamicForms();
	}

	@ApiOperation(value = "Update Form", nickname = "UpdateFormbyName")
	@PatchMapping("/update")
	public ResponseEntity<Response> updateData(@RequestBody DynamicFormDto dynamicFormDto) {

		return service.updateDynamicForm(dynamicFormDto);

	}

}
