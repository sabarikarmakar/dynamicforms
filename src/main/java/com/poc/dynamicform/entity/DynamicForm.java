package com.poc.dynamicform.entity;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.poc.dynamicform.enums.Status;
import com.poc.dynamicform.model.Fields;

import lombok.Data;

@Document
@Data
public class DynamicForm {

	@Id
	private String _id;

	@Field
	private String name;

	private Status status;

	private List<Fields> field;
}
