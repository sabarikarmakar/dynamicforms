package com.poc.dynamicform.entity;

import java.util.List;

import com.poc.dynamicform.enums.Status;
import com.poc.dynamicform.model.Fields;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Document
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SubmitForms {

	@Id
	private String formId;

	private String submittedBy;

	private Status status;

	private String formName;

	private List<Fields> field;

}
