package com.poc.dynamicform.repo;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;

import com.poc.dynamicform.entity.DynamicForm;

@Repository
@EnableMongoRepositories
public interface DynamicRepo extends MongoRepository<DynamicForm, String> {

	DynamicForm findByName(String name);

	@Query(value = "{'status':'ENABLED'}")
	List<DynamicForm> getAllDynamicForms();

	boolean existsDynamicFormByName(String name);

}
