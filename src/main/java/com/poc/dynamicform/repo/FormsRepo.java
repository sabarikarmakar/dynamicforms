package com.poc.dynamicform.repo;

import java.util.List;
import java.util.Optional;

import com.poc.dynamicform.entity.DynamicForm;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;

import com.poc.dynamicform.entity.SubmitForms;

@Repository
@EnableMongoRepositories
public interface FormsRepo extends MongoRepository<SubmitForms, String> {

	@Query(value = "{'status':'ENABLED'}")
	List<SubmitForms> getAllSubmittedForms();

	public SubmitForms findByFormId(String id);

}
