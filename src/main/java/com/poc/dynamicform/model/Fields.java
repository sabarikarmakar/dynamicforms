package com.poc.dynamicform.model;

import java.util.List;

import com.poc.dynamicform.enums.Type;

import lombok.Data;

@Data
public class Fields {

	private String name;

	private String title;

	private Type type;

	private int maxLength;

	private int minLength;

	private boolean isChecked;

	private String enteredValue;

	private String value;

	private List<WidgetData> widgetData;

	private boolean isRow; // by default false

	private List<Fields> childField;

}
