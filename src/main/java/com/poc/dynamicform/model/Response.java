package com.poc.dynamicform.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response {
	private int responseCode;
	private String status;
	private String description;
	private Object data;


}