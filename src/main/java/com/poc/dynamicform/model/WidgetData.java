package com.poc.dynamicform.model;

import lombok.Data;

@Data
public class WidgetData {

	private boolean isSelected;
	private String title;
	private String value;

}
